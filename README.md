DELOFFRE Ludovic Licence 2 Groupe 1 
Projet Assembleur Semestre 4 

Compilateur basé sur le langage PASCAL
Compilateur réalisé à l'aide des TPs de M.JOURLIN

SYNTAXE : 

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Identifier {"," Identifier} "]"
// StatementPart := Statement {";" Statement} "."

// Statement := AssignementStatement
// Instruction qui nécéssite un ';' à la fin de sa ligne
Exemple : a := a + 1;

// AssignementStatement := Identifier ":=" Expression
// Déclaration d'une variable à l'aide de " :="
Exemple : a := 1;

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// Test qui retourne un booléen 
Exemple : 1==1 -> TRUE // 1==2 -> FALSE

// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}
// Identifier := Letter {(Letter|Digit)}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

STRUCTURE DE CONTROLE : 

IF : "IF" Expression "THEN" Statement ["ELSE" Statement];
// Boucle de condition avec procédure à suivre si la condition est vérifié
Exemple : IF a==1 THEN a := a +1 ELSE a := a + 5;

WHILE : "WHILE" Expression "DO" Statement
// Boucle de condition à répétition tant que la condidion n'est pas vérifié
Exemple : WHILE a<10 DO a := a + 1;

FOR : "FOR" AssignementStatement "TO" Expression "DO" Statement 
Exemple : FOR a := 1 TO 15 DO a := a +1;

FOR (avec DOWNTO) : "FOR" AssignementStatement "DOWNTO" Expression "DO" Statement
// Boucle For décrémentale 
Exemple : FOR a := 15 DOWNTO 1 DO a := a -1;

BLOCK : "BEGIN" Statement { ";" Statement } "END"

DISPLAY : DISPLAY Statement 
// Affiche un statement
Exemple : DISPLAY a;

SWITCH : "SWITCH" Expression "CASE" Expression "DO" Statement [ "CASE" Expression "DO" Statement ];
// Vérifie pour chaque cas (CASE) si la condidion est vrai
Exemple : 
SWITCH a
CASE 1
DO 
b := b+1
CASE 2
DO
b := b+8
