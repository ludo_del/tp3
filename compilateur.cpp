//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <map>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE {INT, BOOL};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; 
// This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
//set<string> DeclaredVariables;
map<string, enum TYPE> DeclaredVariables;

unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
TYPE Identifier(void){
	TYPE type1;
	type1 = INT;
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	//cout << type1 <<endl;
	return type1;
}

TYPE Number(void){
	TYPE type1;
	type1 = INT;
	
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return type1;
}

TYPE Expression(void);	// Called by Term() and calls Term()

TYPE Factor(void){
	TYPE type1;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type1 = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
			return type1;
	}
	else {
		if (current==NUMBER)
			Number();
	     	else
				if(current==ID)
					Identifier();
				else
					Error("'(' ou chiffre ou lettre attendue");
					
					}
					TYPE type2;
					type2 = INT;
					return type2;
		
	
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	TYPE type1;
	TYPE type2;
	OPMUL mulop;
	type1 = Factor();
	//cout << type1 <<endl;
	while(current==MULOP){
		mulop=MultiplicativeOperator();	
		type2 = Factor();
		if(type1 != type2){
			cout << "Les types ne sont pas compatibles"<<endl;
		}
		else{
			
			cout << "\tpop %rbx"<<endl;	// get first operand
			cout << "\tpop %rax"<<endl;	// get second operand
			
			switch(mulop){
				case AND:
				
					if(type1 == BOOL and type2 == BOOL){
						cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
						cout << "\tpush %rax\t# AND"<<endl;	// store result
						break;
					}
					
				case MUL:
					if(type1 != BOOL and type2 != BOOL){
						cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
						cout << "\tpush %rax\t# MUL"<<endl;	// store result
						break;
					}
				case DIV:
					if(type1 != BOOL and type2 != BOOL){
						cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
						cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
						cout << "\tpush %rax\t# DIV"<<endl;		// store result
						break;
					}
				case MOD:
					if(type1 != BOOL and type2 != BOOL){
						cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
						cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
						cout << "\tpush %rdx\t# MOD"<<endl;		// store result
						break;
					}
				default:
					Error("opérateur multiplicatif attendu");
			}
		}
	}
	return type1;
	
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	TYPE type1;
	TYPE type2; 
	OPADD adop;
	type1 = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type2 = Term();
		if(type1 != type2){
			cout << "Les types ne sont pas compatibles"<<endl;
		}
		else{
			cout << "\tpop %rbx"<<endl;	// get first operand
			cout << "\tpop %rax"<<endl;	// get second operand
			switch(adop){
				case OR:
					if(type1 == BOOL && type2 == BOOL){
						cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
						break;		
					}	
				case ADD:
					if(type1 != BOOL && type2 != BOOL){
						cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
						break;	
					}		
				case SUB:	
					if(type1 != BOOL && type2 != BOOL){
						cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
						break;
					}
				default:
					Error("opérateur additif inconnu");
			}
			cout << "\tpush %rax"<<endl;			// store result
		}
	}
	
	//cout << type1 <<endl;
	return type1;

}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	
	set<string> idents;
	
	if(current!=RBRACKET) 
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert(make_pair(lexer->YYText(),INT));	
		
	//cout << lexer->YYText() << endl;

	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		//idents.insert(lexer->YYText());
		DeclaredVariables.insert(make_pair(lexer->YYText(),INT));	
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	
	
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}


// Expression := SimpleExpression [RelationalOperator SimpleExpression]
/*
TYPE Expression(void){
	OPREL oprel;
	TYPE type1 = SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		TYPE type2 = SimpleExpression();
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		if(type2!=type1)
			Error("Les Types sont incompatibles");
			
		else{
			cout<<"\tfldl	(%rsp)\t"<<endl;
			cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t# 2x pop nothing"<<endl;
			cout<<"\tfcomip %st(1)\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\tfaddp %st(1)\t# pop nothing"<<endl;
		}

		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		if(type1!=type2){
			
		return BOOL;
		}	
	}
	return type1;
	
}
*/
TYPE Expression(void){
    OPREL oprel;
    TYPE type1;
    type1 = SimpleExpression();
	//cout << type1 <<endl;
    if(current==RELOP){

        oprel=RelationalOperator();
        TYPE type2;
        
        type2 = SimpleExpression();
        //cout << type2 <<endl;
        //cerr<<"TEST"<<endl;
        cout << "\tpop %rax"<<endl;
        cout << "\tpop %rbx"<<endl;
        cout << "\tcmpq %rax, %rbx"<<endl;
        switch(oprel){
            case EQU:
                cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;

                break;
            case DIFF:
                cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;

                break;
            case SUPE:
                cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;

                break;
            case INFE:
                cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;

                break;
            case INF:
                cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;

                break;
            case SUP:
                cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;

                break;
            default:
                Error("Opérateur de comparaison inconnu");
        }
        cout << "\tpush $0\t\t# False"<<endl;
        cout << "\tjmp Suite"<<TagNumber<<endl;
        cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;
        cout << "Suite"<<TagNumber<<":"<<endl;

    if(type1!=type2){
        Error("même type attendu");
    }
    else{
        TYPE type3;
        type3=BOOL;

        return type3;
    }

    }

    return type1;
}
// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void){
    
    TYPE type1;
    TYPE type2;
    
    string variable;
    
    if(current!=ID) 
		Error("Identificateur attendu");
        
	//cout << lexer->YYText()<<endl;
    if(!IsDeclared(lexer->YYText())){
		//cout << "salut"<<endl;
        cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
        exit(-1);
    }
    variable=lexer->YYText();
    
    //cout << variable << endl;
    type1=DeclaredVariables[variable];
    //cout << type1 << endl;
    current=(TOKEN) lexer->yylex();
    
    
    if(current!=ASSIGN)
        Error("caractères ':=' attendus");
    current=(TOKEN) lexer->yylex();
    type2=Expression();
	//cout << type2 <<endl;
	if(type2!=type1)
    {
		
        Error("types incompatibles ");
    }
    cout << "\tpop "<<variable<<endl;
    return variable;
}

void DISPLAYStatement(void){
	int tagNbr=++TagNumber;
	cout<<"DISPLAY"<<tagNbr<<" :"<<endl;
	if(strcmp(lexer->YYText(),"DISPLAY")!=0)
		Error("DISPLAY attendu");
	current=(TOKEN) lexer->yylex();
	TYPE type=Expression();
	if (type==INT){
		cout<<"\tpop %rdx                     # The value to be displayed"<<endl;
		cout<<"\tmovq $FormatString1, %rsi    # \"%llu\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	__printf_chk@PLT"<<endl;
	}
	else if(type==BOOL){
		cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
		cout << "\tcmpq $0, %rdx"<<endl;
		cout << "\tje False"<<tagNbr<<endl;
		cout << "\tmovq $TrueString, %rsi\t# \"TRUE\\n\""<<endl;
		cout << "\tjmp Next"<<tagNbr<<endl;
		cout << "False"<<tagNbr<<":"<<endl;
		cout << "\tmovq $FalseString, %rsi\t# \"FALSE\\n\""<<endl;
		cout << "Next"<<tagNbr<<":"<<endl;
		cout << "\tmovl	$1, %edi"<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	__printf_chk@PLT"<<endl;
	}

	else{
		Error("Caractère 'DISPLAY' attendu");
	}
	
}

void Statement(void);


void IFStatement(void){
	unsigned int tagNbr = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "IF")==0){
		cout << "IF" << tagNbr << ":" <<endl;
		current=(TOKEN) lexer->yylex();
		Expression();
		
		cout << "\tpop %rax "<<endl;
		cout << "\tcmpq $0, %rax" <<endl;
		cout << "\tjz ELSE"<< tagNbr<<endl;
		
		if(current == KEYWORD && strcmp(lexer->YYText(), "THEN")==0){
			cout << "THEN" << tagNbr << ":"  <<endl;
			current=(TOKEN) lexer->yylex();
			Statement();
			
			cout << "\tjmp ENDIF" << tagNbr << endl;
			cout << "ELSE" << tagNbr << ":" << endl;
		
			if(current == KEYWORD && strcmp(lexer->YYText(), "ELSE")==0){
			cout << "ELSE" << tagNbr << ":" <<endl;
			current=(TOKEN) lexer->yylex();
			Statement();
			}
		}
		else{
		Error("Caractère 'THEN' attendu");
	}
	}
	else{
		Error("Caractère 'IF' attendu");
	}
	
	cout << "ENDIF" << tagNbr << ":" <<endl;
}

void SWITCHStatement(void){
	unsigned int tagNbr = ++TagNumber;
	string valeurSwitch;
	string valeurCase;
	
	int switchNbr = tagNbr;
	
	if(current == KEYWORD && strcmp(lexer->YYText(), "SWITCH")==0){
		
		current=(TOKEN) lexer->yylex();
		
		valeurSwitch = lexer->YYText();

		cout << "SWITCH" << switchNbr << ":" <<endl;
		Expression();
		cout << "\tpop " << valeurSwitch << endl;
		
		//current=(TOKEN) lexer->yylex();
		//cout <<"\tpush " << valeurSwitch << endl;
		//cout << "\tjmp CASE "<< caseNbr << endl;
		
		if(current == KEYWORD && strcmp(lexer->YYText(), "CASE")==0){
			
			while(current == KEYWORD && strcmp(lexer->YYText(), "CASE")==0){
				
				current=(TOKEN) lexer->yylex();
				
				
				valeurCase = lexer->YYText();
				cout << "CASE" << tagNbr << ":" << endl;
				
				Expression();

				cout << "\tpush " << valeurSwitch << endl;
				cout << "\tpop %rax" << endl;
				//cout <<"\tpush " << valeurCase << endl;
				cout << "\tpop %rbx" << endl;
				cout << "\tcmpq %rax, %rbx" <<endl;
				cout << "\tjne ENDCASE"<< tagNbr << endl;
				
				if(current == KEYWORD && strcmp(lexer->YYText(), "DO")==0){
					cout << "DO" << tagNbr << ":"  <<endl;
					current=(TOKEN) lexer->yylex();
					
					Statement();
					cout << "\tjmp ENDSWITCH"<< switchNbr <<endl;
					cout << "ENDCASE" << tagNbr++ << ":" << endl;
				
					//cout << "\tjmp ENDSWITCH" << tagNbr << endl;
				}
			
				
				//Statement();
				//current=(TOKEN) lexer->yylex();
				//cout << "\tjz DO" << tagNbr << endl;
				//cout << "\tjmp ENDSWITCH"<< tagNbr << endl;
				
				
			}
			//cout << "\tjmp ENDSWITCH" << tagNbr << endl;
		}
		
		else{
			Error("Caractère 'CASE' attendu");
		}
	}
	else{
		Error("Caractère 'SWITCH' attendu");
	}
	
	
	cout << "ENDSWITCH" << switchNbr << ":" << endl;
}


void WHILEStatement(void){
	unsigned int tagNbr = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "WHILE")==0)
	{	
		cout << "WHILE"<< tagNbr << ":" <<endl;
		current=(TOKEN) lexer->yylex();
		Expression();
		cout << "\tpop %rax" << endl;
		cout << "\tcmpq $0, %rax" <<endl;
		cout << "\tjz ENDWHILE" << tagNbr << endl;
		
		if(current == KEYWORD && strcmp(lexer->YYText(), "DO")==0){
			cout << "DO"<< tagNbr << ":" <<endl;
			current=(TOKEN) lexer->yylex();
			Statement();
			
		}
		
		else{
			Error("Caractère 'DO' attendu");
		}
	}
	else{
		Error("Caractère 'WHILE' attendu");
	}
	cout << "\tjmp WHILE" << tagNbr << endl;
	cout << "ENDWHILE" << tagNbr << ":" <<endl;
}

void FORStatement(void){
	unsigned int tagNbr = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "FOR")==0){ 
		cout << "FOR"<< tagNbr << ":" <<endl;
		current = (TOKEN) lexer -> yylex();
		
		string var = AssignementStatement();
		
		if((current == KEYWORD && strcmp(lexer->YYText(), "TO")==0) || (current == KEYWORD && strcmp(lexer->YYText(), "DOWNTO")==0)){
			
			string op = "addq";
			
			if(current == KEYWORD && strcmp(lexer->YYText(), "DOWNTO")==0){
				op = "subq";
				
			}
			cout << "TO"<< tagNbr << ":" <<endl;
			current = (TOKEN) lexer -> yylex();
			Expression();
			
			cout<<"\tpush "<<var<<endl;
			
            cout << "\tpop %rax"<<endl;
            cout << "\tpop %rbx"<<endl;
            
       
			cout << "\tcmpq %rax, %rbx" <<endl;
            
            cout << "\tje FIN"<<tagNbr<<endl;
            
			if(current == KEYWORD && strcmp(lexer->YYText(), "DO")==0){
				
				cout << "DO"<< tagNbr << ":" <<endl;
				current = (TOKEN) lexer -> yylex();
				Statement();
				
				cout<<"\tpush "<<var<<endl;
                cout << "\tpop %rax"<<endl;
                cout << "\t" << op << " $1, %rax" << endl;
                cout<<"\tpush %rax"<<endl;
                cout<<"\tpop "<<var<<endl;
                cout<<"\tjmp TO"<<tagNbr<<endl;
                
			}
			else{
				Error("Caractère 'DO' attendu");
			}
		}
		
		else{
			Error("Caractère 'TO' attendu ");
		}
		
		cout<<"FIN"<< tagNbr<<":"<<endl;
	}
	else{
		Error("Caractère 'FOR' attendu");
	}

}

void BlockStatement(void){
	unsigned int tagNbr = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "BEGIN")==0){
		cout << "BEGIN"<< tagNbr << ":" <<endl;
		current = (TOKEN) lexer -> yylex();
		Statement();
		cout << "{" << endl;
		if(current == SEMICOLON && strcmp(lexer->YYText(), ";")==0){
			cout << ";" <<endl;
			current = (TOKEN) lexer -> yylex();
			Statement();
			cout << "}" <<endl;
			if(current == KEYWORD && strcmp(lexer-> YYText(), "END")==0){
				cout << "END"<< tagNbr << ":" <<endl;
				current = (TOKEN) lexer -> yylex();
			}
			
			else{
				Error("END attendu");
			}
		}
		else{
			Error(" ; attendu");
		}
	}
	else{
		Error("BEGIN attendu");
	}
	
}

// Statement := AssignementStatement
void Statement(void){
	
	if(current==ID){
		AssignementStatement();
	}
	else if(strcmp(lexer->YYText(), "IF")==0){
		IFStatement();
	}
	else if(strcmp(lexer->YYText(), "WHILE")==0){
		WHILEStatement();
	}
	else if(strcmp(lexer->YYText(), "FOR")==0){
		FORStatement();
	}
	else if(strcmp(lexer->YYText(), "BEGIN")==0){
		BlockStatement();
	}
	else if(strcmp(lexer->YYText(), "SWITCH")==0){
		SWITCHStatement();
	}
	else if(strcmp(lexer->YYText(), "DISPLAY")==0){
		DISPLAYStatement();
	}
	
	else{
		Error("Le mot clé est inconnu");
	}
	
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << ".data" << endl;
	cout << "FormatString1:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE"<<endl; 

	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





